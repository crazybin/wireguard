FROM alpine:edge
RUN apk upgrade --no-cache
RUN apk add --no-cache s6-overlay
RUN apk add --no-cache wireguard-go --repository=https://dl-cdn.alpinelinux.org/alpine/edge/testing
COPY /root /
EXPOSE 51820/udp
ENTRYPOINT ["/init"]
