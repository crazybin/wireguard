# Crazybin Wireguard

Crazybin Wireguard is a robust container image that wraps the powerful Wireguard server, enabling secure and efficient VPN connections in your containerized environment. Built on the lightweight and secure Alpine Linux base, this Docker image provides a reliable solution for setting up and managing a Wireguard server effortlessly.

## What is Wireguard

<https://www.wireguard.com/>

WireGuard® is an extremely simple yet fast and modern VPN that utilizes state-of-the-art cryptography. It aims to be faster, simpler, leaner, and more useful than IPsec, while avoiding the massive headache. It intends to be considerably more performant than OpenVPN. WireGuard is designed as a general purpose VPN for running on embedded interfaces and super computers alike, fit for many different circumstances. Initially released for the Linux kernel, it is now cross-platform (Windows, macOS, BSD, iOS, Android) and widely deployable. It is currently under heavy development, but already it might be regarded as the most secure, easiest to use, and simplest VPN solution in the industry.

## Usage

Setting up Crazybin Wireguard is straightforward. To add interfaces, simply mount a volume at /etc/wireguard, which contains the necessary configuration files for wg-quick. This allows for seamless interface management within the container.

### Podman run

``` sh
podman run -d \
  -p 51820:51820/udp \
  -v /path/to/wireguard:/etc/wireguard \
  quay.io/crazybin/wireguard:latest
```

Replace /path/to/wireguard with the path on your host machine where you store the Wireguard configuration files. This command maps the necessary volume and exposes the Wireguard server on port 51820 for UDP traffic.

### Podman kube

Create config in kube.yml:

``` yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: configs
data:
  wg0.conf: |
    [Interface]
    Address = 10.200.100.8/24
    ListenPort = 51820
    DNS = 10.200.100.1
    PrivateKey = oK56DE9Ue9zK76rAc8pBl6opph+1v36lm7cXXsQKrQM=

    [Peer]
    PublicKey = GtL7fZc/bLnqZldpVofMCD6hDjrK28SsdLxevJ+qtKU=
    PresharedKey = /UwcSPg38hW/D9Y3tcS1FOV0K1wuURMbS0sesJEP5ak=
    AllowedIPs = 0.0.0.0/0
    Endpoint = demo.wireguard.com:51820
---
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: main
  name: main
spec:
  containers:
  - image: quay.io/crazybin/wireguard
    name: wireguard
    ports:
    - containerPort: 51820
      hostPort: 51820
      protocol: UDP
    securityContext:
      capabilities:
        add:
        - CAP_NET_ADMIN
    volumeMounts:
    - name: dev-tun
      mountPath: /dev/net/tun
    - name: configs
      mountPath: /etc/wireguard
  volumes:
  - name: dev-tun
    hostPath:
      path: /dev/net/tun
      type: CharDevice
  - name: configs
    configMap:
      name: configs
```

Play config:

``` sh
podman kube play kube.yml
```

## Join the Crazybin community

Crazybin Wireguard is an open-source project hosted on GitLab. You can find the source code, contribute, and explore additional information, documentation, and updates by visiting our GitLab repository:

<https://gitlab.com/crazybin/wireguard>

Experience secure VPN connections with Crazybin Wireguard - the secure Wireguard server container image based on Alpine Linux. Enhance your containerized environment with ease and secure your network traffic today!

Elevate your network security with Crazybin Wireguard!
